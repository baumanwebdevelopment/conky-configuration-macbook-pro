![conky.png](https://bitbucket.org/repo/AxLXBX/images/2957562332-conky.png)

# Conky configuration #

This is a configuration file for Conky running on a dual boot (Mac OS, Linux) MacBook Pro with a Retina display (resolution 2560px x 1600px).
Conky is a system monitor (and more). Conky is free software and runs in X on Linux and BSD. Website: [http://conky.sourceforge.net/aboutconky.html](http://conky.sourceforge.net/aboutconky.html)

# Install #

First install and start Conky. See the [Conky documentation](http://conky.sourceforge.net/documentation.html).
After that put the conky.conf in a place Conky can find it.

# Author #

Arthur Bauman  
[LinkedIn](https://www.linkedin.com/in/arthurbauman)  
[Website](http://bauman.nu)